# lightspeed-test

### Link: http://lstest.surge.sh

### To start:

```bash
$ npm install
```

### To develop:

```bash
$ npm run dev
```

### To build for production:

```bash
$ npm run build
```


---

## Tools
- [vuepack](https://github.com/egoist/vuepack) : A modern starter which uses Vue 2, Vuex, Vue-router and Webpack 2
- [SASS](http://sass-lang.com/) : css preprocessor
- ES6
- [WebStorm](https://www.jetbrains.com/webstorm/?fromMenu) : JavaScript IDE
- [Surge](https://surge.sh/) : Static web publishing

## Implemented Functions

- data source: (http://beta.json-generator.com/api/json/get/4kiDK7gxZ)
- Create grid of items, each item has "thumbnail, price & add to cart button"
- Clicking on any item will open an popup with more data "description, price, remaining stock & add to cart/remove button"
- "add to cart" button will do the following:
  - item will be added to the cart
  - stock number of the item will be updated in the details view
- "remove" button will do the following:
  - item will be removed from the cart
  - stock number of the item will be updated in the details view
- "checkout" button will show a successful message and clear the cart

## Notes

- I added the "remove" button to complete the cycle of purchase.
- I tried to make it looks nice and also works right
