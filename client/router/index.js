import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      components: {
        top: require('../components/header.vue'),
        default: require('../components/home.vue')
      }
    }
  ]
})
