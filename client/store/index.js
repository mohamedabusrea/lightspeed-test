import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  products: [
    {
      "description": "Proident adipisicing excepteur non ad enim deserunt. Sint magna exercitation cupidatat enim. Sit dolore magna consectetur commodo.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$1,003.45",
      "stock": {
        "remaining": 23
      },
      "_id": "571762bfec3aac46241599e0"
    },
    {
      "description": "Dolor incididunt mollit ad aliqua proident non. Excepteur aute exercitation eu sint laborum nostrud anim duis nisi consequat. Elit Lorem irure deserunt elit commodo cillum. Adipisicing veniam exercitation tempor pariatur quis. Culpa cupidatat sit laborum cupidatat. Tempor magna laborum non incididunt culpa do anim do.",
      "color": "brown",
      "image": "http://placehold.it/100x100",
      "price": "$1,161.57",
      "stock": {
        "remaining": 11
      },
      "_id": "571762bf4c6b9203d1ff13a3"
    },
    {
      "description": "Velit anim ex anim nostrud amet magna sint esse laborum incididunt ullamco. Duis mollit Lorem occaecat sint nostrud et nulla nisi qui labore duis. Sint non quis quis velit voluptate. Proident cillum irure labore excepteur sunt.",
      "color": "brown",
      "image": "http://placehold.it/100x100",
      "price": "$190.63",
      "stock": {
        "remaining": 43
      },
      "_id": "571762bf452db4e27a7becb7"
    },
    {
      "description": "Quis veniam nulla Lorem aute eiusmod culpa. Ut aliquip laboris mollit laborum commodo fugiat deserunt excepteur deserunt esse adipisicing laboris. Voluptate ullamco anim excepteur cillum consectetur veniam anim quis dolor voluptate.",
      "color": "blue",
      "image": "http://placehold.it/100x100",
      "price": "$2,655.30",
      "stock": {
        "remaining": 32
      },
      "_id": "571762bf554305b2ae596d03"
    },
    {
      "description": "Esse et sunt eu Lorem irure velit. Nulla anim magna pariatur sit officia occaecat labore aliqua enim. Non culpa ut culpa duis sit veniam.",
      "color": "brown",
      "image": "http://placehold.it/100x100",
      "price": "$348.04",
      "stock": {
        "remaining": 31
      },
      "_id": "571762bf6cadb683cce0649d"
    },
    {
      "description": "Ex in ut qui proident nisi qui consectetur fugiat aliqua qui sit laborum do sit. Labore nisi quis excepteur pariatur incididunt minim pariatur non nulla aute. Voluptate anim adipisicing consectetur labore reprehenderit velit excepteur eu. Aute id voluptate officia ad cupidatat voluptate quis cupidatat nisi do.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$1,334.57",
      "stock": {
        "remaining": 61
      },
      "_id": "571762bfb94c6dc82ab4f8b1"
    },
    {
      "description": "Eiusmod ut nostrud consequat exercitation non sit exercitation. Aute aute labore minim proident sunt commodo minim labore. Esse sunt cupidatat non tempor esse eiusmod proident ea cupidatat esse. In et anim fugiat ad proident adipisicing do voluptate proident in nisi eiusmod anim excepteur.",
      "color": "brown",
      "image": "http://placehold.it/100x100",
      "price": "$1,648.18",
      "stock": {
        "remaining": 29
      },
      "_id": "571762bfef680f19e46a5a06"
    },
    {
      "description": "In nisi cillum consequat dolor excepteur. Sunt nisi ipsum laborum adipisicing incididunt. Ad aliqua anim adipisicing nisi sunt mollit exercitation cupidatat consectetur Lorem esse non exercitation ullamco. Voluptate elit do nulla sunt excepteur nulla in Lorem Lorem deserunt ut sint labore sint.",
      "color": "brown",
      "image": "http://placehold.it/100x100",
      "price": "$453.85",
      "stock": {
        "remaining": 17
      },
      "_id": "571762bf11936a6e4cb6b934"
    },
    {
      "description": "Mollit mollit occaecat officia veniam sit aliquip nisi ad nostrud anim. Sunt magna id irure incididunt incididunt et eiusmod ex officia. Adipisicing sit laborum laborum minim culpa qui enim commodo.",
      "color": "blue",
      "image": "http://placehold.it/100x100",
      "price": "$564.70",
      "stock": {
        "remaining": 16
      },
      "_id": "571762bfe788b08d868d39e5"
    },
    {
      "description": "Tempor sunt aliqua nisi duis consequat sit nisi proident aute occaecat culpa. Id non irure officia adipisicing eiusmod sit nostrud commodo dolore anim quis magna sint. Officia ut id elit nulla dolor fugiat consectetur aliqua dolore. Laborum reprehenderit exercitation culpa aliquip pariatur ex proident non qui proident nisi amet labore dolore. Ullamco culpa et elit cupidatat do consectetur occaecat sunt. Nulla consectetur et adipisicing voluptate consectetur sunt mollit exercitation incididunt excepteur aute.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$1,967.64",
      "stock": {
        "remaining": 57
      },
      "_id": "571762bfccff5bb9a9d7a539"
    },
    {
      "description": "Aute Lorem officia occaecat duis sint pariatur reprehenderit nisi in excepteur laborum. Eu adipisicing in excepteur ea fugiat ullamco nostrud irure non cillum. Pariatur commodo ad ea excepteur ipsum irure incididunt dolore quis. Non tempor enim quis Lorem dolor. Voluptate commodo incididunt culpa fugiat. Amet proident do anim nisi. Quis et fugiat mollit nisi irure id excepteur fugiat elit eiusmod sint.",
      "color": "brown",
      "image": "http://placehold.it/100x100",
      "price": "$381.89",
      "stock": {
        "remaining": 2
      },
      "_id": "571762bf25bb7003becc416d"
    },
    {
      "description": "Consequat consequat enim sit incididunt culpa cillum incididunt deserunt cupidatat. Ad consequat enim tempor nulla nulla culpa enim eiusmod laborum labore. Ex ipsum pariatur dolore excepteur Lorem nostrud non occaecat ex duis.",
      "color": "green",
      "image": "http://placehold.it/100x100",
      "price": "$974.52",
      "stock": {
        "remaining": 73
      },
      "_id": "571762bfd95fc4119f6ba8f9"
    },
    {
      "description": "Voluptate velit esse ad aute qui enim minim enim mollit voluptate quis. Voluptate non et non consectetur magna enim. Officia nostrud aute tempor aliquip tempor ipsum. Commodo reprehenderit incididunt sint incididunt occaecat dolore qui. Fugiat cillum duis nostrud anim minim in culpa dolore. Ad anim ex voluptate nisi.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$2,102.71",
      "stock": {
        "remaining": 82
      },
      "_id": "571762bf5ce781445f91b615"
    },
    {
      "description": "Aliqua esse cupidatat in in quis irure quis anim aliqua ipsum. Elit sit aliqua eu cillum nulla laborum amet ullamco laboris. Sit nostrud est aute ex est in ut id exercitation excepteur ea anim. Anim laboris occaecat consectetur adipisicing qui deserunt voluptate. Eu elit ex aliquip anim proident. Deserunt non enim exercitation ex mollit laboris tempor eu pariatur elit.",
      "color": "pink",
      "image": "http://placehold.it/100x100",
      "price": "$2,676.60",
      "stock": {
        "remaining": 54
      },
      "_id": "571762bf29ffb77e3b7b9d26"
    },
    {
      "description": "Excepteur duis nulla incididunt voluptate eu eiusmod laboris ad irure pariatur nisi. Aliquip laboris ullamco dolor duis ut qui pariatur et voluptate. Dolore non ex ex excepteur dolore et nisi non ut nostrud eu non. Non aliqua amet voluptate labore sint proident Lorem proident tempor occaecat nisi sunt cupidatat esse. Non anim nisi ipsum ea.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$935.14",
      "stock": {
        "remaining": 3
      },
      "_id": "571762bf05a0ce170cfd946b"
    },
    {
      "description": "Dolore Lorem Lorem irure nisi ex nisi. Eu occaecat pariatur duis magna cillum fugiat in. Quis pariatur eiusmod dolor ad. Voluptate non ut commodo enim ex duis cupidatat anim dolore mollit velit reprehenderit anim ea. Tempor ut id velit consectetur consequat magna aliqua eu esse ipsum minim. Sunt eu incididunt et aliqua reprehenderit.",
      "color": "blue",
      "image": "http://placehold.it/100x100",
      "price": "$1,836.17",
      "stock": {
        "remaining": 52
      },
      "_id": "571762bfb7779bcc2f0cdfbb"
    },
    {
      "description": "Mollit non aliquip aute aliquip duis aliqua nostrud cupidatat duis ea. Excepteur reprehenderit excepteur veniam pariatur velit velit. Est ea et adipisicing quis esse. Minim ut incididunt ex laboris duis. Nulla exercitation aute ipsum cupidatat ex amet aliquip reprehenderit ipsum sit cupidatat.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$1,323.83",
      "stock": {
        "remaining": 2
      },
      "_id": "571762bf1078b0e875f36882"
    },
    {
      "description": "Elit enim nostrud reprehenderit consequat aliqua. Pariatur consequat dolor anim ad in ad magna aute veniam sint laboris. Excepteur culpa duis dolore nisi non id nulla. Voluptate magna duis nisi officia deserunt nostrud sit in sit culpa. Sit tempor ipsum velit duis veniam. In nostrud laborum eu esse ut cillum in cupidatat mollit. Ipsum quis mollit magna esse magna exercitation est ex culpa qui sint velit officia labore.",
      "color": "blue",
      "image": "http://placehold.it/100x100",
      "price": "$262.75",
      "stock": {
        "remaining": 49
      },
      "_id": "571762bfcde16b9821e7ca5c"
    },
    {
      "description": "Mollit eu aliqua irure exercitation ut duis nisi labore fugiat. Dolor ullamco ipsum voluptate officia enim reprehenderit officia Lorem in cupidatat exercitation labore amet dolor. Commodo Lorem nulla id commodo tempor aliquip eu ex culpa elit magna.",
      "color": "blue",
      "image": "http://placehold.it/100x100",
      "price": "$1,454.20",
      "stock": {
        "remaining": 100
      },
      "_id": "571762bf5f688d4c556d1bed"
    },
    {
      "description": "Velit pariatur proident dolore consequat anim cupidatat mollit reprehenderit laboris laborum ad cillum. Ad dolore aliqua irure ex exercitation ad ad. Minim nostrud non commodo proident laboris exercitation esse nisi. Aliqua nulla voluptate laboris ea pariatur eiusmod deserunt ipsum ex. Aliqua aliquip minim ea ullamco ea sit quis fugiat labore est sit magna sint. Elit sint ea dolor laboris laborum in qui adipisicing qui do.",
      "color": "pink",
      "image": "http://placehold.it/100x100",
      "price": "$3,586.62",
      "stock": {
        "remaining": 7
      },
      "_id": "571762bf888f102d50dda9b7"
    },
    {
      "description": "Commodo qui ut anim dolore nostrud proident elit enim eiusmod qui sunt nisi eiusmod. Exercitation ipsum aliqua quis sunt dolore aute sint est do quis quis irure fugiat commodo. Id ex cupidatat et ut.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$3,820.82",
      "stock": {
        "remaining": 65
      },
      "_id": "571762bf6ba82bd6bc277a22"
    },
    {
      "description": "Cillum consectetur do consequat incididunt veniam non esse aute labore velit ut voluptate fugiat. Veniam veniam velit ut consequat aliqua. Aliqua nostrud veniam esse magna qui sunt ut duis ullamco.",
      "color": "yellow",
      "image": "http://placehold.it/100x100",
      "price": "$3,460.65",
      "stock": {
        "remaining": 96
      },
      "_id": "571762bf7676f007179160e8"
    },
    {
      "description": "Ex ut non est nisi ipsum officia labore. Incididunt sit velit occaecat dolor deserunt deserunt irure veniam est magna. Est aute sit ullamco cupidatat reprehenderit aliquip irure proident ut fugiat qui. Aute id sint aliqua veniam nulla consequat tempor ea ipsum excepteur tempor pariatur nisi. Et laboris esse id eu est aute labore eu minim.",
      "color": "green",
      "image": "http://placehold.it/100x100",
      "price": "$2,539.14",
      "stock": {
        "remaining": 73
      },
      "_id": "571762bfe8ae745886d52683"
    },
    {
      "description": "Sunt in do sit sit ad minim adipisicing cillum do. Non mollit proident adipisicing nulla. Ad reprehenderit ullamco commodo reprehenderit consequat laborum esse est ullamco minim irure incididunt. Nulla non consequat anim esse esse minim minim.",
      "color": "blue",
      "image": "http://placehold.it/100x100",
      "price": "$2,399.56",
      "stock": {
        "remaining": 90
      },
      "_id": "571762bf92e7159f97e63c33"
    },
    {
      "description": "Lorem id mollit anim pariatur velit ea aute culpa id. Incididunt cillum proident elit dolor cupidatat in ad dolore aute qui eiusmod enim esse ullamco. Nulla deserunt laborum adipisicing tempor reprehenderit.",
      "color": "pink",
      "image": "http://placehold.it/100x100",
      "price": "$258.59",
      "stock": {
        "remaining": 68
      },
      "_id": "571762bf181961c042d448d3"
    }
  ],
  cartValue: {
    total: 0,
    productsInCart: []
  }
}

const mutations = {
  addToCart(state, productID){
    let productIndex = state.products.findIndex(p => p._id == productID),
      product = state.products[productIndex],
      productPrice = state.products[productIndex].price.replace(',', '').split('$')[1],
      cartValue = state.cartValue;

    if (product.stock.remaining > 0) {
      //ADD ITEM TO CART
      cartValue.total += parseFloat(productPrice);
      product.stock.remaining -= 1;

      productIndex = cartValue.productsInCart.findIndex(p => p.id == productID); //check if the product is already purchased

      if (productIndex >= 0)
        cartValue.productsInCart[productIndex].quantity += 1; //PRODUCT IS ALREADY PURCHASED
      else
        cartValue.productsInCart.push({id: productID, quantity: 1}) //FIRST TIME TO BE PURCHASED
    }
  },
  removeFromCart(state, productID){
    let productIndex = state.products.findIndex(p => p._id == productID),
      product = state.products[productIndex],
      productPrice = state.products[productIndex].price.replace(',', '').split('$')[1],
      cartValue = state.cartValue;

    if (product.stock.remaining >= 0) {
      //REMOVE ITEM TO CART
      cartValue.total -= parseFloat(productPrice);
      product.stock.remaining += 1;
      //CHECK IF THE PRODUCT IS ALREADY PURCHASED
      productIndex = cartValue.productsInCart.findIndex(p => p.id == productID);
      if (cartValue.productsInCart[productIndex].quantity == 1)
        Vue.delete(cartValue.productsInCart, productIndex);
      else
        cartValue.productsInCart[productIndex].quantity -= 1;
    }
  },
  checkout(state){
    let products = state.products,
      cartValue = state.cartValue;

    cartValue.total = 0;
    cartValue.productsInCart = [];
  }
}

const actions = {
  incrementAsync ({commit}) {
    setTimeout(() => {
      commit('INCREMENT')
    }, 200)
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
